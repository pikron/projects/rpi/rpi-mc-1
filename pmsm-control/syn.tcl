# synplify_pro -licensetype synplifypro_actel -batch syn.tcl

project -new rpi_pmsm_control
impl -name syn0

add_file pll50to200.vhd
add_file util.vhdl
add_file qcounter.vhdl
add_file dff.vhdl
add_file mcpwm.vhdl
add_file cnt_div.vhdl
add_file adc_reader.vhdl
add_file dff3.vhdl
add_file div128.vhdl
add_file div256.vhdl

# top-level
add_file rpi_pmsm_control.vhdl
add_file rpi_pmsm_control.sdc

set_option -technology IGLOO
set_option -part AGL250V5
set_option -package VQFP100
set_option -speed_grade std

set_option -frequency 50.0

project -run
project -save
