-- provides frequency division by 256 (8 bit divider)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util.all;

entity div256 is

port (
	clk_in: in std_logic;
	div256: out std_logic
);
end div256;


architecture behavioral of div256 is
	signal count : std_logic_vector (8 downto 0);
begin
	
	div256 <= count(8);
	
	seq : process 
	begin
		wait until (clk_in'event and clk_in='1');
		count <= std_logic_vector(unsigned(count) + 1);
	end process;

	
		
end behavioral;

