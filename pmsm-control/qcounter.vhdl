--
-- * Quadrature Signal Decoder *
-- Used for IRC sensor interfacing
--
-- (c) 2010 Marek Peca <hefaistos@gmail.com>
--
-- license: GNU LGPL and GPLv3+
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity qcounter is
port (
	clock: in std_logic;
	reset: in std_logic;
	a0, b0: in std_logic;
	qcount: out std_logic_vector (31 downto 0);
	a_rise, a_fall, b_rise, b_fall, ab_event: out std_logic;
	ab_error: out std_logic
);
end qcounter;

architecture behavioral of qcounter is
	component dff
	port (
		clock: in std_logic;
		d: in std_logic;
		q: out std_logic
	);
	end component;

	subtype std_logic4 is std_logic_vector (3 downto 0);
	signal a, b, a_prev, b_prev: std_logic;
	signal count_prev: std_logic_vector (29 downto 0)
		:= "000000000000000000000000000000";
	signal count: std_logic_vector (29 downto 0);
begin
	--ustaleni signalu a mezi tiky hodin
	--reaguje na nabeznou hranu
	dff_a: dff
	port map (
		clock => clock,
		d => a0,
		q => a
	);
	
	--ustaleni signalu b mezi tiky hodin
	--reaguje na nabeznou hranu
	dff_b: dff
	port map (
		clock => clock,
		d => b0,
		q => b
	);
  
	--prvni dva rady kombinacne
	qcount(0) <= a xor b;
	qcount(1) <= b;
	qcount(31 downto 2) <= count;
  
	--k cemu tento prosess? jen pro prenos udalosti na piny?
	comb_event: process (a_prev, b_prev, a, b) --proc je v sensitivity listu i stary stav?? jen kvuli nulovani?
	begin
		a_rise <= '0';
		a_fall <= '0';
		b_rise <= '0';
		b_fall <= '0';
		ab_event <= '0';
		ab_error <= '0';
		if ((a xor a_prev) and (b xor b_prev)) = '1' then -- a i b se zmenily zaroven
			-- forbidden double transition
			ab_error <= '1';
		else
			a_rise <= (a xor a_prev) and a; --a nabezna
			a_fall <= (a xor a_prev) and not a; --a sestupna
			b_rise <= (b xor b_prev) and b; --b nabezna
			b_fall <= (b xor b_prev) and not b; --b sestupna
			ab_event <= (a xor a_prev) or (b xor b_prev); --a nebo b se zmenily
		end if;
	end process;

	--prechod do 3. radu (binarne)
	comb_count: process (a_prev, b_prev, a, b, count,count_prev) --proc jsou v sensitivity prev stavy?
	begin
		if (a_prev = '0') and (b_prev = '1') and (a = '0') and (b = '0') then --posun dopredu 
			count <= count_prev + 1;
		elsif (a_prev = '0') and (b_prev = '0') and (a = '0') and (b = '1') then --posun dozadu
			count <= count_prev - 1;
		else
			count <= count_prev;
		end if;
	end process;

	--s nabeznou hranou hodin ulozime stare (prev) hodnoty, pripadne synchronni reset
	--proc resuteje count_prev misto count?? neprojevi se tato zmena az pri pruchodu comb_count??
	seq: process
	begin
		wait until clock'event and clock = '1';
		if reset = '1' then
			count_prev <= "000000000000000000000000000000";
		else
			count_prev <= count;
		end if;
		a_prev <= a;
		b_prev <= b;
	end process;
	
end behavioral;
--Poznamky
	--proc pouzivan count_prev a neprirazuji rovnou do count?
