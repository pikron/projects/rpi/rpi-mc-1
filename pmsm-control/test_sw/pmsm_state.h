#ifndef PMSM_STATE
#define PMSM_STATE

#include <stdint.h>
#include <semaphore.h>

#define OLD_POS_NUM 	50
#define MAX_DUTY	170
#define MAX_SPEED 	(7*OLD_POS_NUM)

#define LOG_ROWS	11
#define LOG_DEF_COL	8000
#define MAX_LOGS	8000
#define LOG_PERIOD	2

struct rpi_in;

struct rpi_state{
	//const unsigned MAX_DUTY;	/*Max duty*/

	struct rpi_in* spi_dat;		/* spi data */
	sem_t thd_par_sem;		/* data metual exlusion access */
	uint8_t test;			/* configuratin byte - pwm enabl. bits etc. */
	uint32_t tf_count;		/* number of transfer*/

	uint16_t index_dist;		/* distance to index position */
	unsigned char index_ok;		/* we have actual index position */

	uint16_t pwm1, pwm2, pwm3;	/* pwm duty cycles*/
	uint16_t t_pwm1, t_pwm2, t_pwm3;/* debug*/

	int duty;			/* duty cycle of pwm */
	int desired_pos;		/* desired position */
	int old_pos[OLD_POS_NUM];	/* old  positions */
	int desired_spd;		/* desired speed */
	int speed;

	char commutate;			/* zapina prepocet duty na jednotlive pwm */
	char pos_reg_ena;		/* position regulation enable */
	char spd_reg_ena;		/* speed rugulation enable */
	unsigned short alpha_offset;	/* offset between 'alpha' and 'a' axis */

	int spd_err_sum;		/* for speed pid regulator */

	int *logs[LOG_ROWS];		/* logs */
	int log_col_count;		/* number of log columns */
	int log_col;			/* current colimn */
	char doLogs;			/* schall we make logs? */

	void (*main_commutator)(struct rpi_state*);	/* primarni komutator */
	void (*main_controller)(struct rpi_state*);	/* primarni regulator */

	char error;			/* detekce chyboveho stavu */

	int8_t h1_old,h2_old,h3_old;	/* stare hodnoty hallu */
};

/**
 * Index OK.
 */
inline void setIndexOK(struct rpi_state*);

/**
 * Index Lost.
 */
inline void setIndexLost(struct rpi_state*);

/**
 * Turn commutation on.
 */
inline void setCommutationOn(struct rpi_state*);

/**
 * Turn commutation off.
 */
inline void setCommutationOff(struct rpi_state*);

/**
 * Turn on speed regulation.
 */
inline void setRegulationSpeed(struct rpi_state*);

/**
 * \brief Turn on position regulation
 */
inline void setRegulationPos(struct rpi_state*);

/**
 * \brief Turn off regulation.
 */
inline void setRegulationOff(struct rpi_state*);

#endif /*PMSM_STATE*/
