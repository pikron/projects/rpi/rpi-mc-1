#include "pmsm_state.h"
#include "commutators.h"
#include "controllers.h"

/**
 * Index Lost.
 */
inline void setIndexLost(struct rpi_state* this){
	this->index_ok=0;
	this->main_commutator=simple_hall_commutator;
}

/**
 * Index OK.
 */
inline void setIndexOK(struct rpi_state* this){
	this->index_ok=1;
	this->main_commutator=inv_trans_comm_2;
}

/**
 * Turn commutation on.
 */
inline void setCommutationOn(struct rpi_state* this){
	this->commutate=1;
	if (this->index_ok){
		this->main_commutator=inv_trans_comm_2;
	}else{
		this->main_commutator=simple_hall_commutator;
	}
}

/**
 * Turn commutation off.
 */
inline void setCommutationOff(struct rpi_state* this){
	this->commutate=0;
	this->main_commutator=zero_commutator;
}

/**
 * Turn on speed regulation.
 */
inline void setRegulationSpeed(struct rpi_state* this){
	this->spd_reg_ena=1;
	this->pos_reg_ena=0;
	this->main_controller=spd_pid;
}

/**
 * \brief Turn on position regulation
 */
inline void setRegulationPos(struct rpi_state* this){
	this->spd_reg_ena=0;
	this->pos_reg_ena=1;
	this->main_controller=pos_pid;
}

/**
 * \brief Turn off regulation.
 */
inline void setRegulationOff(struct rpi_state* this){
	this->spd_reg_ena=0;
	this->pos_reg_ena=0;
	this->main_controller=zero_controller;


}
