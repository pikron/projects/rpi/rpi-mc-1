/**
 * \brief Implementace pomocnych funkci.
 * \file misc.c
 * \date Feb 1, 2015
 * \author Martin Prudek
 *
 * 	Implementace pomocnych funkci.
 * 	Vetsinou pro komunikaci mezi vlakny, jejich sychronizaci a predavani priorit.
 */


#include <sched.h>
#include <signal.h>	/*signal handler*/
#include <stdlib.h>
#include <stdio.h> 	/*printf*/
#include <pthread.h>
#include <sys/mman.h> 	/*mlockall*/

#include "misc.h"



/**
 * \brief
 * Signal handler.
 */
void appl_sig_handler(int sig){
	exit(0); /*atexit is set*/
}

/*
 * \brief Setup initial environment.
 */
void setup_environment(){
	/*struktura pro signal handler*/
	struct sigaction sighnd;

	if (mlockall(MCL_FUTURE | MCL_CURRENT) < 0) {
	    fprintf(stderr, "mlockall failed - cannot lock application in memory\n");
	    exit(1);
	 }

	atexit(appl_stop);

	/*nastaveni signalu pro vypnuti pomoci Ctrl+C*/
	sighnd.sa_handler=appl_sig_handler;
	sigaction(SIGINT, &sighnd, NULL );
}

/*
 * \brief
 * Creates RT thread
 */
int create_rt_task(pthread_t *thread, int prio, void *(*start_routine) (void *), void *arg){
	int ret ;

	pthread_attr_t attr;
	struct sched_param schparam;

	/*inicializace implicitnich atributu*/
	if (pthread_attr_init(&attr) != 0) {
		fprintf(stderr, "pthread_attr_init failed\n");
		return -1;
	}

	/*nastavi dedeni planovace z attr*/
	if (pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0) {
		fprintf(stderr, "pthread_attr_setinheritsched failed\n");
		return -1;
	}

	/*nastaveni planovace*/
	if (pthread_attr_setschedpolicy(&attr, SCHED_FIFO) != 0) {
		fprintf(stderr, "pthread_attr_setschedpolicy SCHED_FIFO failed\n");
		return -1;
	}

	schparam.sched_priority = prio;

	/*nastavit atribut attr podle hodnoty schparam*/
	if (pthread_attr_setschedparam(&attr, &schparam) != 0) {
		fprintf(stderr, "pthread_attr_setschedparam failed\n");
		return -1;
	}

	/*vytvori vlakno*/
	ret = pthread_create(thread, &attr, start_routine, arg);

	/*uvolni strukturu, nema vliv na vlakna jiz vytvorena*/
	pthread_attr_destroy(&attr);

	return ret;
}



