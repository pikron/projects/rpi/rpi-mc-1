/*
 *
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <string.h>

#include "rp_spi.h" /*struct rpi_in */
#include "pmsm_state.h"

//#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define ARRAY_SIZE 16
static void pabort(const char *s)
{
	perror(s);
	abort();
}

static const char *device = "/dev/spidev0.1";
static uint8_t mode = 0;
static uint8_t bits = 8;
static uint32_t speed = 500000;
static uint16_t delay = 0;
static int fd;

/**
 * \brief Testovaci funkce. Odesle same nuly. Delka 64bit. Vysledek vypise.
 */
void transfer()
{
	int ret;
	uint8_t tx[]={0,0,0,0,0,0,0,0} ;/*= {
		0x00, 0x00, 0x01, 0x23, 0x45, 0x67,
		0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23,
		0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF,
		0x01, 0x23, 0x45, 0x67, 0x89, 0xAB,
		0xCD, 0xEF
	};/**/
	printf("ARRAY_SIZE=%d\n",ARRAY_SIZE);
	uint8_t rx[ARRAY_SIZE] = {0, }; /* inicializace vsech prvku na nulu? */
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = ARRAY_SIZE,
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};

	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	if (ret < 1)
		pabort("can't send spi message");

	for (ret = 0; ret < ARRAY_SIZE; ret++) {
		if (!(ret % 6))
			puts("");
		printf("%.2X ", rx[ret]);
	}
	puts("");
}
void spi_read(struct rpi_state* this)
{
	uint8_t *uint8_p;
	uint16_t tmp;
	int sign;
	struct rpi_in in;
	int ret;

	uint8_t rx[ARRAY_SIZE] = {0, }; /* inicializace vsech prvku na nulu */
	uint8_t tx[ARRAY_SIZE] = {0, }; /* inicializace vsech prvku na nulu */
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = ARRAY_SIZE,
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
	};

	memset(&in,0,sizeof(in));

	/*Data format:
	 * tx[4] - bity 95 downto 88 - bits that are sent first
	 * tx[5] - bity 87 downto 80
	 * tx[6] - bity 79 downto 72
	 * tx[7] - bity 71 downto 64
	 * tx[8] - bity 63 downto 56
	 * tx[9] - bity 55 downto 48
	 * tx[10] - bity 47 downto 40
	 * tx[11] - bity 39 downto 32
	 * tx[12] - bity 31 downto 24
	 * tx[13] - bity 23 downto 16
	 * tx[14] - bity 15 downto 8
	 * tx[15] - bity 7 downto 0
	 *
	 * bit 95 - ADC reset
	 * bit 94 - enable PWM1
	 * bit 93 - enable PWM2
	 * bit 92 - enable PWM3
	 * bit 91 - shutdown1
	 * bit 90 - shutdown2
	 * bit 89 - shutdown3
	 * 	.
	 * 	.
	 * 	Unused
	 * 	.
	 * 	.
	 * bits 47 .. 32 - match PWM1
	 * bits 31 .. 16 - match PWM2
	 * bits 15 .. 0  - match PWM3
	 */



	/* keep the 11-bit cap*/

	if (this->pwm1>2047) this->pwm1=2047;
	if (this->pwm2>2047) this->pwm2=2047;
	if (this->pwm3>2047) this->pwm3=2047;

	tx[0]=this->test; /*bit 94 - enable PWM1*/

	/*now we have to switch the bytes due to endianess */
	/* ARMv6 & ARMv7 instructions are little endian */
	/*pwm1*/
	tx[10]=((uint8_t*)&this->pwm1)[1]; /*MSB*/
	tx[11]=((uint8_t*)&this->pwm1)[0]; /*LSB*/

	/*pwm2*/
	tx[12]=((uint8_t*)&this->pwm2)[1]; /*MSB*/
	tx[13]=((uint8_t*)&this->pwm2)[0]; /*LSB*/

	/*pwm3*/
	tx[14]=((uint8_t*)&this->pwm3)[1]; /*MSB*/
	tx[15]=((uint8_t*)&this->pwm3)[0]; /*LSB*/

	/*----------------------------------------*/

	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);


	if (ret < 1){
		pabort("can't send spi message");
	}

	/*-----------------------------------------*/


	for (ret = 0; ret < ARRAY_SIZE; ret++) {
		this->spi_dat->debug_rx[ret]=rx[ret];
	}

	/*/

	/*prichozi data:
	 * rx[0] - bity 127 downto 120 the first income bit..127
	 * rx[1] - bity 119 downto 112
	 * rx[2] - bity 111 downto 104
	 * rx[3] - bity 103 downto 96
	 * rx[4] - bity 95 downto 88
	 * rx[5] - bity 87 downto 80
	 * rx[6] - bity 79 downto 72
	 * rx[7] - bity 71 downto 64
	 * rx[8] - bity 63 downto 56
	 * rx[9] - bity 55 downto 48
	 * rx[10] - bity 47 downto 40
	 * rx[11] - bity 39 downto 32
	 * rx[12] - bity 31 downto 24
	 * rx[13] - bity 23 downto 16
	 * rx[14] - bity 15 downto 8
	 * rx[15] - bity 7 downto 0	the last income bit..0
	 */

	/*uprava endianity pozice*/
	uint8_p=(uint8_t*)&this->spi_dat->pozice_raw;
	/* x86 je Little-Endian */
	uint8_p[0]=rx[3]; /* LSB */
	uint8_p[1]=rx[2];
	uint8_p[2]=rx[1];
	uint8_p[3]=rx[0]; /*MSB*/ /*with sign bit*/

	/*halove sondy
	 * hal1 - bit95
	 * hal2 - bit94
	 * hal3 - bit93
	 */
	this->spi_dat->hal1=!!(0x80 & rx[4]);
	this->spi_dat->hal2=!!(0x40 & rx[4]);
	this->spi_dat->hal3=!!(0x20 & rx[4]);

	/* index position
	 * bits 92 downto 81
	 * 	92..88 in rx[4] last 5 bits (from left)
	 * 	87..81 in rx[5] first 7 bits (from left)
	 */
	this->spi_dat->index_position=0x1F & rx[4];
	this->spi_dat->index_position<<=8;
	this->spi_dat->index_position|=0xFE & rx[5];
	this->spi_dat->index_position>>=1;

	/* current measurments count
	 * bits 80 downto 72
	 * bit 80 in rx[5]
	 * bits 79..72 in rx[6]
	 */

	this->spi_dat->adc_m_count=0x01 & rx[5];
	this->spi_dat->adc_m_count<<=8;
	this->spi_dat->adc_m_count|=rx[6];


	/** currents
	 * ch2 - bits 71 downto 48
	 * 	71..64 in rx[7] - all byte
	 * 	63..56 in rx[8] - all byte
	 * 	55..48 in rx[9] - all byte
	 * ch0 - bits 47 downto 24
	 * 	47..40 in rx[10] - all byte
	 * 	39..32 in rx[11] - all byte
	 * 	31..24 in rx[12] - all byte
	 * ch1 - bits 23 downto 0
	 * 	23..16 in rx[13] - all byte
	 * 	15..8 in rx[14] - all byte
	 * 	7..0 in rx[15] - all byte
	 */

	this->spi_dat->ch2=rx[7];
	this->spi_dat->ch2<<=8;
	this->spi_dat->ch2|=rx[8];
	this->spi_dat->ch2<<=8;
	this->spi_dat->ch2|=rx[9];

	this->spi_dat->ch0=rx[10];
	this->spi_dat->ch0<<=8;
	this->spi_dat->ch0|=rx[11];
	this->spi_dat->ch0<<=8;
	this->spi_dat->ch0|=rx[12];

	this->spi_dat->ch1=rx[13];
	this->spi_dat->ch1<<=8;
	this->spi_dat->ch1|=rx[14];
	this->spi_dat->ch1<<=8;
	this->spi_dat->ch1|=rx[15];



}

int spi_init()
{
	int ret = 0;

	fd = open(device, O_RDWR);
	if (fd < 0)
		pabort("can't open device");
	printf("device open\n");
	/*
	 * spi mode
	 */
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		pabort("can't get spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		pabort("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		pabort("can't get max speed hz");

	printf("spi mode: %d\n", mode);
	printf("bits per word: %d\n", bits);
	printf("delay: %d\n", delay);
	printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);


	return ret;
}

void spi_disable(){
	close(fd);

}
