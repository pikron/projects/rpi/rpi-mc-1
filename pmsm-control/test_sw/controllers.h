/**
 * \brief
 * Speed and position regulators.
 * \author Martin Prudek
 * \file controllers.h
 *
 */
#ifndef CONTROLLERS
#define CONTROLLERS

#include "pmsm_state.h"	/*pmsm state*/
#include "rp_spi.h"	/*spi struct*/

#define PID_P		0.3

/* 	RL-tool results from first order system approx	*/
/*	P=0.16 I=13,4/freq=0.013			*/
/*	Hodnoty upraveny podle skutecnych vysledku	*/
/*	P=0.8	I=0.01					*/

/* PID_P_S		0.16 */

#define PID_P_S		0.08 		/*2.3 kmita*/ /*1.4 vhodne jen pro P regulator*/
#define PID_I_S		0.0134

/**
 * \brief No regulation.
 */
inline void zero_controller(struct rpi_state*);

/**
 * \brief
 * Very simple position P regulator.
 * Now only with P-part so that the error doesnt go to zero.
 */
void pos_pid(struct rpi_state*);

/**
 * \brief
 * Very simple PI speed regulator.
 */
void spd_pid(struct rpi_state*);


#endif /*CONTROLLERS*/
