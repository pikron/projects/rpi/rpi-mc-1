/**
 * \brief
 * Duty cycle commutation.
 * \author Martin Prudek
 * \file commutators.h
 *
 */
#ifndef COMMUTATORS
#define COMMUTATORS

#include "pmsm_state.h"
#include "rp_spi.h"

/**
 * \brief
 * Simple vector-control commutator without delta-transformation.
 * Nearly same as sin_commuatator.
 */
void inv_trans_comm(struct rpi_state*);

/*
 * \brief
 * Robust vector-control commuator with Delta-transformation.
 */
void inv_trans_comm_2(struct rpi_state*);

/**
 * \brief
 * Simple voltage commutation, takes use of sin function.
 */
void sin_commutator(struct rpi_state*);

/*
 * \brief
 * Switches PWM's at point where they produce same force.
 * This points are found from IRC position,
 */
void simple_ind_dist_commutator(struct rpi_state*);

/*
 * \brief
 * Switches PWM's at point where they produce same force
 */
void simple_hall_commutator(struct rpi_state*);

/*
 * \brief
 * No commutation.
 */
void zero_commutator(struct rpi_state*);


#endif /*COMMUTATORS*/
