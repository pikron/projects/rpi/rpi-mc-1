/**
 * \file rp_spi.h
 */
#ifndef RP_SPI
#define RP_SPI
#include <stdint.h> /*uint32_t*/

/**
 * \brief Struktura pro prichozi data z fpga.
 */
struct rpi_in{
	signed long int pozice;		/*continue with normal size and test it..*/
	uint32_t pozice_raw;		/*with offset*/
	uint32_t ch0, ch1, ch2;
	int8_t hal1,hal2,hal3; 		/* bool values */
	int8_t en1, en2, en3; 		/*(bool)last read pwm-enable values - !they are changed after reading ! */
	int8_t shdn1,shdn2,shdn3; 	/*(bool)last read shutdown values - !they are changed after reading ! */
	uint16_t adc_m_count;		/*current measurments count*/
	uint8_t debug_rx[16];
	uint16_t index_position;	/* raw position of irc_i */
};

struct rpi_state;

/**
 * \brief Testovaci funkce.
 */
void transfer(void);

/**
 * \brief Inicializace pro spi.
 */
int spi_init(void);

/**
 * \brief Uzavreni spi.
 */
void spi_disable(void);

void spi_read(struct rpi_state*);

#endif /*RP_SPI*/
