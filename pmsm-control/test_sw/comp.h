/**
 * \brief
 * Computatations of position, index, speed.
 *
 */
#ifndef COMP
#define COMP

struct rpi_in;
struct rpi_state;

/**
 * \brief
 * Substact initial position.
 */
void substractOffset(struct rpi_in* data, struct rpi_in* offset);

/**
 * \brief
 * Computation of distance to index.
 *
 * K dispozici je 12-bit index, to umoznuje ulozit 4096 ruznych bodu
 * Je nutne vyjadrit 1999 bodu proti i posmeru h.r. od indexu -
 * 	to je 3999 bodu
 * 	=>12 bitu je dostacujicich, pokud nikdy nedojde ke ztrate
 * 		signalu indexu
 */
void comIndDist(struct rpi_state*);

/*
 * \brief
 * Computates speed.
 */
void compSpeed(struct rpi_state*);

#endif /*COMP*/

