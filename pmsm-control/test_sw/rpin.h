/**
 * \brief Interface pro ovladani pinu na RPi.
 * \file rpin.h
 * \date March 21, 2015
 * \author Martin Prudek
 *
 * 	Deklaruje nativni funkce pro ovladani pinu a pwm na RPi.
 * 	Implementace  "rpi_hw.c"
 *
 *
 */

#ifndef RPIN_H_
#define RPIN_H_


/* Pin modes */
#define	INPUT			 0
#define	OUTPUT			 1
#define	PWM_OUTPUT		 2
#define	GPIO_CLOCK		 3
#define	FSEL_INPT		0b000	/*0*/
#define	FSEL_OUTP		0b001	/*1*/
#define	FSEL_ALT0		0b100	/*4*/
#define	FSEL_ALT1		0b101	/*5*/
#define	FSEL_ALT2		0b110	/*6*/
#define	FSEL_ALT3		0b111	/*7*/
#define	FSEL_ALT4		0b011	/*3*/
#define	FSEL_ALT5		0b010	/*2*/

/* Logic levels */
#define	LOW			 0
#define	HIGH			 1

/* Clk sources */
enum ClkSource{
	PLLD_500_MHZ=0,		/*500 MHz*/
	OSC_19_MHZ_2=1,		/*19.2 MHz*/
     	HDMI_216_MHZ=2,		/*216 MHz*/
     	PLLC_1000_MHZ=3 	/*1000 MHz, changes with overclock*/
};

/**
 * \brief inicializuje pouziti GPIO pinu...
 * (namapuje registry DMA)
 */
extern int initialise (void);

/**
 * \brief
 *	Select the native "balanced" mode, or standard mark:space mode
 */
extern void pwmSetMode (int);

/**
 * \brief
 *	Set the PWM range register. We set both range registers to the same
 *	value.
 */
extern void pwmSetRange (unsigned int);

/**
 * \brief
 *	Set/Change the PWM clock.
 *	Not tested.
 */
extern void pwmSetClock (int);

/**
 * \brief nastavi mod dnaeho pinu
 * \param pin [in] cislo GPIO pinu
 * \param mode [in] mod pinu -> in/out/altx
 *
 * Nastavi pin jako vstupni / vystupni, pripadne nastavi slternativni funkci.
 */
extern void pinMode (int, int);

/**
 * \brief Precte logickou uroven daneho pinu 0/1
 * v pripade neicnicializovaneho GPIO nebo neexistence pinu vrati 0
 * \param pin [in] cislo GPIO pinu
 */
extern int digitalRead (int);

/**
 * \brief zapise logickou uroven na dany pin
 * \param pin [in] cislo GPIO pinu
 * \param value [in] logicka uroven 0/1
 */
extern void digitalWrite (int, int);

/**
 * \brief nastavi hodnotu pwm
 * \param pin[in] cislo GPIO PWM pinu
 * \param value[int] sirka plneni  pwm (max=1024)
 */
extern void pwmWrite (int, int);

/**
 * \brief A different approach to set gpio mode.
 */
extern void gpioSetMode(unsigned gpio_n, unsigned mode);

/**
 * \brief Initialize gpclk.
 */
extern int initClock(enum ClkSource,int, int);

#endif /*RPIN_H_*/
