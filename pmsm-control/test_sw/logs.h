/**
 * \brief
 * Making data logs.
 */

#ifndef LOGS
#define LOGS

struct rpi_state;

/*
 * \brief
 * Makes log.
 */
void makeLog(struct rpi_state*);

/*
 * \brief
 * Initialize logs
 */
void logInit(struct rpi_state*);

/*
 * \brief
 * Free logs memory.
 */
void freeLogs(struct rpi_state*);

/*
 * \brief
 * Save logs to text file.
 */
void saveLogs(struct rpi_state*);

#endif /*LOGS*/
