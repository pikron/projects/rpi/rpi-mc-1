/**
 * \brief Interface pro klienta UDP.
 * \file udp_cli.h
 * \author Martin Prudek
 * 
 * 	Definice souboru funkci, ktere zajisti cteni polohy vzdaleneho motoru pomoci UDP.
 */

#include <semaphore.h>
#include <stdint.h>

struct remote_pos_st{
	sem_t *semaphore;	/* semafor pro ukladani pozice */
	char * ip;		/* ip zdroje */
	int * rem_pos;		/* adresa, kam ukladat pozici */
	int factor;		/* prepcet mezi pozicemi */
	char stop;		/* indicates request to stop */
};

/**
 * \param tick_count Perioda spousteni= pocet tiku o delce 200us
 * \param ip_ptr Pointer na string definujici ip position serveru  
 * 
 * 	Funkce spusti sekvenci,
 * 	ktera pravidelne aktualizuje pozici vzdaleneho moturu
 * 	port 45789
 */

void* start_reading_remote_position(void*);

