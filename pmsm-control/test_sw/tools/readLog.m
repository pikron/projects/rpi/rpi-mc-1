% cte logy vyutvorene na Raspberry Pi
% 
% Format prichozich dat
%	LOG(1,:) cas
%	LOG(2,:) pozice
%	LOG(3,:) PWM1
%	LOG(4,:) PWM2
%	LOG(5,:) PWM3
%	LOG(6,:) duty
%	LOG(7,:) pozadovana rychlost
%	LOG(8,:) skutecna rychlost
%	LOG(9,:) ch1 (pwm1)
%	LOG(10,:) ch2 (pwm2)
%	LOG(11,:) ch0 (pwm3)
%	
%--------------------------------------------------------------------------
%% Nacitani souboru
%cd('/home/warg/logy');

LOG=dlmread('logs.log');
%LOG=dlmread('spd_old_val_10_duty_step_100.log');
%LOG=dlmread('duty100_step.log');
%LOG=dlmread('spd100_step2.log');





%% jen rychlost
hold on;
plot(LOG(1,:),LOG(7,:),'LineWidth',1);
plot(LOG(1,:),LOG(8,:),'LineWidth',1);
legend('Požadovaná rychlost','Aktuální rychlost','Location','southeast');
title('Aktuální a požadovaná rychlost');
xlabel('Čas [ms]');
ylabel('Rychlost [IRC/50ms]');


%% vykresli vse

limits=[1.65 1.9]*10000;
set_lim=0;
pozice=0;
plotc=3;

fig=figure;

if pozice
    subplot(plotc,1,1);
    hold on;
    plot(LOG(1,:),LOG(2,:),'LineWidth',2);
    legend('pozice');
    title('Závislost polohy na čase');
    xlabel('Čas [ms]');
    ylabel('Poloha IRC');
    if set_lim
        xlim(limits);
    end;
end;

%rychlost

subplot(plotc,1,1);
hold on;
plot(LOG(1,:),LOG(7,:),'LineWidth',1);
plot(LOG(1,:),LOG(8,:),'LineWidth',1);
legend('Požadovaná rychlost','Aktuální rychlost','Location','southeast');
title('Aktuální a požadovaná rychlost');
xlabel('Čas [ms]');
ylabel('Rychlost [IRC/50ms]');
if set_lim
    xlim(limits);
end;

subplot(plotc,1,2);
hold on;
plot(LOG(1,:),LOG(3,:),'LineWidth',1);
plot(LOG(1,:),LOG(4,:),'LineWidth',1);
plot(LOG(1,:),LOG(5,:),'LineWidth',1);
plot(LOG(1,:),LOG(6,:),'LineWidth',1);
legend('pwm1','pwm2','pwm3','šířka plnění','Location','northwest');
title('Šířka plnění a její přepočet na PWM'); 
xlabel('Čas [ms]');
ylabel('Šířka plnění [bity]');
if set_lim
    xlim(limits);
end;

subplot(plotc,1,3);
hold on;
plot(LOG(1,:),LOG(9,:),'LineWidth',1);
plot(LOG(1,:),LOG(10,:),'LineWidth',1);
plot(LOG(1,:),LOG(11,:),'LineWidth',1);
legend('ch1 (pwm1)','ch2 (pwm2)','ch0 (pwm3)','Location','southwest');
title('Záznamy proudů');
xlabel('Čas [ms]');
ylabel('Proud');
if set_lim
    xlim(limits);
end;

%% ulozi

   % set(fig, 'InvertHardcopy', 'off');
    saveas(fig, 'rlt_spd_100_step.pdf');
   
