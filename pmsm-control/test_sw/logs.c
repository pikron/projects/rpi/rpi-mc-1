
#include <stdio.h>
#include <stdlib.h>

#include "logs.h"
#include "pmsm_state.h"
#include "rp_spi.h"

/*
 * \brief
 * Free logs memory.
 */
void freeLogs(struct rpi_state* this){
	int r;
	if (this->log_col_count){
		for (r=0;r<LOG_ROWS;r++){
			free(this->logs[r]);
		}
	}
	this->log_col_count=0;
	this->doLogs=0;
}

/*
 * \brief
 * Initialize logs
 */
void logInit(struct rpi_state* state){
	int r;
	state->log_col=0;
	state->log_col_count=LOG_DEF_COL;
	for (r=0;r<LOG_ROWS;r++){
		state->logs[r]=malloc(state->log_col_count*sizeof(int));
		if (state->logs[r]==NULL){
			state->error=1;
			state->log_col_count=-1;
			return;
		}
	}
	state->doLogs=1;
}

/*
 * \brief
 * Makes log.
 */
void makeLog(struct rpi_state* this){
	int r;
	if (this->log_col==MAX_LOGS-1){
		this->doLogs=0;
		return;
	}
	this->logs[0][this->log_col]=(int)this->tf_count;
	this->logs[1][this->log_col]=(int)this->spi_dat->pozice;

	this->logs[2][this->log_col]=(int)this->pwm1;
	this->logs[3][this->log_col]=(int)this->pwm2;
	this->logs[4][this->log_col]=(int)this->pwm3;
	this->logs[5][this->log_col]=this->duty;

	this->logs[6][this->log_col]=this->desired_spd;
	this->logs[7][this->log_col]=this->speed;

	this->logs[8][this->log_col]=(int)(this->spi_dat->ch1/this->spi_dat->adc_m_count);
	this->logs[9][this->log_col]=(int)(this->spi_dat->ch2/this->spi_dat->adc_m_count);
	this->logs[10][this->log_col]=(int)(this->spi_dat->ch0/this->spi_dat->adc_m_count);

	this->log_col++;
     /*
        if (rps.log_col==rps.log_col_count-1){
        	rps.log_col_count*=2;
        	rps.log_col_count%=MAX_LOGS;
        	for (r=0;r<LOG_ROWS;r++){
        		rps.logs[r]=realloc(rps.logs[r],rps.log_col_count*sizeof(int));
        		if (rps.logs[r]==NULL){
        			rps.doLogs=0;
        			rps.error=1;
        		}
        	}
        }
     */
}

/*
 * \brief
 * Save logs
 */
void saveLogs(struct rpi_state* this){
	FILE *f;
	int r,s;

	f = fopen("logs.log", "w");
	if (f == NULL){
		printf("Error opening file!\n");
		return;
	}

	for (r=0;r<LOG_ROWS;r++){
		for(s=0;s<this->log_col-1;s++){	/*posledni sloupec je vevyplneny*/
			if (s==this->log_col-2){
				fprintf(f,"%d ",this->logs[r][s]);
	                }else{
	                        fprintf(f,"%d, ",this->logs[r][s]);
	                }
	        }
	        fprintf(f,"\r");
	}
	fclose(f);
	freeLogs(this);
}
