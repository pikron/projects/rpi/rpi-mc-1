/**
 * \brief Interface pro pomocne funkce.
 * \file misc.h
 * \date Feb 1, 2015
 * \author Martin Prudek
 */

#ifndef MISC_H_
#define MISC_H_

#ifndef NULL
#define NULL (void*)0
#endif /*NULL*/

/**
 * \brief
 * Setup initial environment.
 */
void setup_environment();

/**
 * \brief
 * Extern handler for signal termination.
 * Deklarovana jako 'extern', protoze ji pouzivaji funkce v misc.c,
 * 	deklarovana je ale jinde. Vetsinou v main file.
 */
extern void appl_stop();

/**
 * \brief
 * Create RT capable thread.
 */
int create_rt_task(pthread_t *thread, int prio, void *(*start_routine) (void *), void *arg);

#endif /* MISC_H_ */
