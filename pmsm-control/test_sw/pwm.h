/**
 * \brief Interface pro ovladani pwm.
 * \file pwm.h
 * \date Jan 21, 2015
 * \author Martin Prudek
 *
 * 	Deklaruje funkce pro ovladani pulzne sirkove modulace bez znalosti konkretniho hardware.
 * 	Implementace pro rt-linux v "rpi_hw.c"
 * 		pro wiringPI ve "wir_Pi_api.c"
 *
 */

#ifndef PWM_H_
#define PWM_H_

/**
 * inicializuje pwm
 */
extern void pwm_init();
/**
 * deinicializuje pwm
 */
extern void pwm_disable();
/**
 * nastavi sirku plneni pwm
 */
extern void pwm_width(int width);

#endif /*PWM_H_*/
