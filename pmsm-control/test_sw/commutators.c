/**
 * \brief
 * Implementation of commutators and transformations.
 */

#include "commutators.h"
#include "pxmc_sin_fixed.h"	/*sinus function*/


/*
 * \brief
 * No commutation.
 */
void zero_commutator(struct rpi_state* this){
	return;
}

/*
 * \brief
 * Computes minimum value of three numbers.
 * Input values must be in range <-2^28;2^28>.
 */
static int32_t min(int32_t x, int32_t y, int32_t z){
        int32_t diff,sign;

        diff=x-y; /*rozdil*/
        sign=(*((uint32_t*)&diff))>>31; /*znamenko -> detekuje, ze y je vetsi*/
        x=y+sign*diff; /*ulozime mensi cislo, pokud sign>0, pak diff<0 */

        diff=x-z; /*rozdil*/
        sign=(*((uint32_t*)&diff))>>31; /*znamenko -> detekuje, ze z je vetsi*/
        x=z+sign*diff; /*ulozime mensi cislo, pokud sign>0, pak diff<0 */

        return x;
}

/*
 * \brief
 * Transformace pro uhel pocitany po smeru hodinovych rucicek
 */
static void dq2alphabeta(int32_t *alpha, int32_t *beta, int d, int q, int32_t sin, int32_t cos){
	*alpha=cos*d+sin*q;
	*beta=-sin*d+cos*q;
	return;
}

/**
 * \brief
 * Zpetna Clarkova transformace
 */
static void alphabeta2pwm3(int32_t * ia, int32_t * ib, int32_t *ic,int32_t alpha, int32_t beta){
	*ia=alpha;
	*ib=-alpha/2+beta*887/1024;
	*ic=-alpha/2-beta*887/1024;
}

/*
 * \brief
 * Preocita napeti na jednotlivych civkach na napeti,
 * 	ktera budou privedena na svorky motoru.
 * 	Tedy na A(yel)-pwm1, B(red)-pwm2, C(blk)-pwm3
 */
static void transDelta(int32_t * u1, int32_t * u2, int32_t *u3, int32_t ub , int32_t uc){
	int32_t t;

	/*vypocte napeti tak, aby odpovidaly rozdily*/
	*u1=uc;
	*u2=uc+ub;
	*u3=0;

	/*najde zaporne napeti*/
	t=min(*u1,*u2,*u3);

	/*dorovna zaporna napeti na nulu*/
	*u1-=t;
	*u2-=t;
	*u3-=t;
}

/**
 * \brief
 * Simple vector-control commutator without delta-transformation.
 * Nearly same as sin_commuatator.
 */
void inv_trans_comm(struct rpi_state* this){
	uint32_t pos;
	int32_t sin, cos;
	int32_t alpha, beta;
	int32_t pwma,pwmb,pwmc;
	pos=this->index_dist;
	/*melo by byt urceno co nejpresneji, aby faze 'a' splyvala s osou 'alpha'*/
	pos+=717;
	/*use it as cyclic 32-bit logic*/
	pos*=4294967;
	pxmc_sincos_fixed_inline(&sin, &cos, pos, 16);
	dq2alphabeta(&alpha, &beta,0,this->duty, sin, cos);
	alpha>>=16;
	beta>>=16;
	alphabeta2pwm3(&pwma,&pwmb, &pwmc,alpha,beta);

	if (pwma<0) pwma=0;
	if (pwmb<0) pwmb=0;
	if (pwmc<0) pwmc=0;

	/*debugovaci vystupy - pouze vypisy hodnot*/
	this->t_pwm1=(uint16_t)pwma;
	this->t_pwm3=(uint16_t)pwmb;
	this->t_pwm2=(uint16_t)pwmc;
}

/*
 * \brief
 * Robust vector-control commuator with Delta-transformation.
 */
void inv_trans_comm_2(struct rpi_state* this){
	uint32_t pos;
	int32_t sin, cos;
	int32_t alpha, beta;
	int32_t ua,ub,uc;
	int32_t ia,ib,ic;
	int32_t u1,u2,u3;
	pos=this->index_dist;

	pos+=this->alpha_offset; /*zarovnani faze 'a' s osou 'alpha'*/

	/*pro výpočet sin a cos je pouzita 32-bit cyklicka logika*/
	pos*=4294967;
	pxmc_sincos_fixed_inline(&sin, &cos, pos, 16);

	dq2alphabeta(&alpha, &beta,0,this->duty, sin, cos);
	alpha>>=16;
	beta>>=16;

	alphabeta2pwm3(&ia,&ib, &ic,alpha,beta);

	ua=ia;
	ub=ib;
	uc=ic;

	transDelta(&u1,&u2, &u3,ub,uc);

	this->pwm1=(uint16_t)u1;
	this->pwm2=(uint16_t)u2;
	this->pwm3=(uint16_t)u3;
}

/**
 * \brief
 * Simple voltage commutation, takes use of sin finction.
 */
void sin_commutator(struct rpi_state* this){
	#define DEGREE_60  	 715827883
	#define DEGREE_120  	1431655765
	#define DEGREE_180  	2147483648
	#define DEGREE_240  	2863311531
	#define DEGREE_300  	3579139413
	uint32_t j,pos;
	int32_t sin;
	int32_t pwm;
	int duty=this->duty;
	pos=this->index_dist;
	/*aby prictene uhly mohla byt kulata cisla, musime index posunout*/
	pos+=38;
	/*use it as cyclic 32-bit logic*/
	pos*=4294967;
	if (this->duty>=0){ 	/*clockwise rotation*/
		/* 1st phase */
		sin = pxmc_sin_fixed_inline(pos+DEGREE_240,10); /*10+1 bity*/ /*-120*/
                pwm=sin*duty/1024;
                if (pwm<0) pwm=0;
                this->pwm1=(uint16_t)pwm;

                /* 2nd phase */
                sin = pxmc_sin_fixed_inline(pos+DEGREE_120,10); /*10+1 bity*/ /*-240*/
                pwm=sin*duty/1024;
                if (pwm<0) pwm=0;
                this->pwm2=(uint16_t)pwm;

                /* 3rd phase */
                sin = pxmc_sin_fixed_inline(pos,10); /*10+1 bity*/
                pwm=sin*duty/1024;
                if (pwm<0) pwm=0;
                this->pwm3=(uint16_t)pwm;
        }else{
        	duty=-duty;

		/* 1st phase */
		sin = pxmc_sin_fixed_inline(pos+DEGREE_60,10); /*10+1 bity*/ /*-300*/
		pwm=sin*duty/1024;
		if (pwm<0) pwm=0;
		this->pwm1=(uint16_t)pwm;

                /* 2nd phase */
                sin = pxmc_sin_fixed_inline(pos+DEGREE_300,10); /*10+1 bity*/ /*-60-*/
                pwm=sin*duty/1024;
                if (pwm<0) pwm=0;
                this->pwm2=(uint16_t)pwm;

                /* 3rd phase */
                sin = pxmc_sin_fixed_inline(pos+DEGREE_180,10); /*10+1 bity*/ /*-180*/
                pwm=sin*duty/1024;
                if (pwm<0) pwm=0;
                this->pwm3=(uint16_t)pwm;
        }
}

/*
 * \brief
 * Test function to be placed in controll loop.
 * Switches PWM's at point where they produce same force.
 * This points are found thanks to IRC position,
 */
void simple_ind_dist_commutator(struct rpi_state* this){
	int duty=this->duty;
	uint16_t index_dist=this->index_dist;
	if (duty>=0){ /* clockwise - so that position increase */
		/* pwm3 */
		if ((index_dist>=45 && index_dist<=373) ||
		(index_dist>=1048 && index_dist<=1377)){
			this->pwm1=0;
			this->pwm2=0;
			this->pwm3=duty;
			/* pwm1 */
		}else if ((index_dist>=373 && index_dist<=711) ||
		(index_dist>=1377 && index_dist<=1711)){
			this->pwm1=duty;
			this->pwm2=0;
			this->pwm3=0;
			/* pwm2 */
		}else if ((index_dist>=0 && index_dist<=45) ||
		(index_dist>=711 && index_dist<=1048) ||
		(index_dist>=1711 && index_dist<=1999)){
			this->pwm1=0;
			this->pwm2=duty;
			this->pwm3=0;
		}
	}else{	/*counter-clockwise - position decrease */
		/* pwm3 */
		if ((index_dist>=544 && index_dist<=881) ||
		(index_dist>=1544 && index_dist<=1878)){
			this->pwm1=0;
			this->pwm2=0;
			this->pwm3=-duty;
			/* pwm1 */
		}else if ((index_dist>=0 && index_dist<=211) ||
		(index_dist>=881 && index_dist<=1210) ||
		(index_dist>=1878 && index_dist<=1999)){
			this->pwm1=-duty;
			this->pwm2=0;
			this->pwm3=0;
			/* pwm2 */
		}else if ((index_dist>=211 && index_dist<=544) ||
		(index_dist>=1210 && index_dist<=1544)){
			this->pwm1=0;
			this->pwm2=-duty;
			this->pwm3=0;
		}
	}
}

/*
 * \brief
 * Test function to be placed in controll loop.
 * Switches PWM's at point where they produce same force
 */
void simple_hall_commutator(struct rpi_state* this){
	int duty=this->duty;
	int8_t hal1=this->spi_dat->hal1;
	int8_t hal2=this->spi_dat->hal2;
	int8_t hal3=this->spi_dat->hal3;
	if (duty>=0){ /* clockwise - so that position increase */
		/* pwm3 */
		if (hal2 && !hal3){
			this->pwm1=0;
			this->pwm2=0;
			this->pwm3=duty;
			/* pwm1 */
		}else if (hal1 && !hal2){
			this->pwm1=duty;
			this->pwm2=0;
			this->pwm3=0;
			/* pwm2 */
		}else if (!hal1 && hal3){
			this->pwm1=0;
			this->pwm2=duty;
			this->pwm3=0;
		}
	}else{	/*counter-clockwise - position decrease */
		/* pwm3 */
		if (!hal2 && hal3){
			this->pwm1=0;
			this->pwm2=0;
			this->pwm3=-duty;
			/* pwm1 */
		}else if (!hal1 && hal2){
			this->pwm1=-duty;
			this->pwm2=0;
			this->pwm3=0;
			/* pwm2 */
		}else if (hal1 && !hal3){
			this->pwm1=0;
			this->pwm2=-duty;
			this->pwm3=0;
		}
	}
}
