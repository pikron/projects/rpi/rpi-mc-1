RPi and rpi-mc-1 Board Pins
===========================

| RPi GPIO | Pin no. | Simple-dc | PMSM SPI | PMSM AM | RPI JTAG |
| -------- | ------- | --------- | -------- | ------- |--------- |
| +3.3v    | 1       |           |          |         |          |
| +5v      | 2       |           |          |         |          |
| gpio2    | 3       |           |          |         |          |
| +5v      | 4       |           |          |         |          |
| gpio3    | 5       |           |          |         |          |
| GND      | 6       |           |          |         |          |
| gpio4    | 7       |           | gpio_clk | gpio_clk| TDI      |
| gpio14   | 8       | TxD       | TxD      | TxD     | TxD      |
| GND      | 9       |           |          |         |          |
| gpio15   | 10      | RxD       | RxD      | RxD     | RxD      |
| gpio17   | 11      |           |          | irc_i   |          |
| gpio18   | 12      | pwm_in    |          | pwm1    |          |
| gpio27   | 13      | irc_b (n) |          |         | TMS      |
| GND      | 14      |           |          |         |          |
| gpio22   | 15      | pwm_dir_in|          | pwm1_en | TRST     |
| gpio23   | 16      | irc_a     |          | irc_a   | RTCK     |
| +3.3v    | 17      |           |          |         |          |
| gpio24   | 18      | irc_a     |          |         | TDO      |
| gpio10   | 19      |           | spi_mosi | spi_mosi|          |
| GND      | 20      |           |          |         |          |
| gpio9    | 21      |           | spi_miso | spi_miso|          |
| gpio25   | 22      | irc_b (n) |          | irc_b   | TCK      |
| gpio11   | 23      |           | spi_clk  | spi_clk |          |
| gpio8    | 24      | irc_b (o) |          |         |          |
| GND      | 25      |           |          |         |          |
| gpio7    | 26      | irc_b (o) | spi_ce   | spi_ce  |          |
| ID_SDA   | 27      |           |          |         |          |
| ID_SCL   | 28      |           |          |         |          |
| gpio5    | 29      |           |          |         |          |
| GND      | 30      |           |          |         |          |
| gpio6    | 31      |           |          |         |          |
| gpio12   | 32      |           |          | pwm3    |          |
| gpio13   | 33      |           |          | pwm2    |          |
| GND      | 34      |           |          |         |          |
| gpio19   | 35      |           |          | pwm2_en |          |
| gpio16   | 36      |           |          |         |          |
| gpio26   | 37      |           |          | pwm3_en |          |
| gpio20   | 38      |           |          |         |          |
| GND      | 39      |           |          |         |          |
| gpio21   | 40      |           |          |         |          |

