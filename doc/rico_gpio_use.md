AM43xx RICO and rpi-mc-1 Board Pins
===================================

| Reg   | Function     | J10/J11| Pin no. | Simple-dc | PMSM SPI | PMSM AM |
|-------|--------------|--------| ------- | --------- | -------- | ------- |
|       |              |        | 1       |           |          |         |
|       |              |        | 2       |           |          |         |
|       |              |        | 3       |           |          |         |
|       |              |        | 4       |           |          |         |
|       |              |        | 5       |           |          |         |
|       |              |        | 6       |           |          |         |
|       | CLKOUT2      | J10-25 | 7       |           | gpio_clk | gpio_clk|
|       |              |        | 8       | TxD       | TxD      | TxD     |
|       |              |        | 9       |           |          |         |
|       |              |        | 10      | RxD       | RxD      | RxD     |
| 1A8.1 | MCASP0_AxR1  | J11-38 | 11      |           |          | irc_i   |
| 04C.6 | MMC2_DAT1    | J11-33 | 12      | pwm_in    |          | pwm1    |
|       |              |        | 13      | irc_b (n) |          |         |
|       |              |        | 14      |           |          |         |
| 044.1 | MMC2_DAT0    | J11-31 | 15      | pwm_dir_in|          | pwm1_en |
| 1A0.1 | MCASP0_ACLKR | J11-30 | 16      | irc_a     |          | irc_a   |
|       |              |        | 17      |           |          |         |
|       |              |        | 18      | irc_a     |          |         |
| 154.0 | SPI0_D0      | J10-19 | 19      |           | spi_mosi | spi_mosi|
|       |              |        | 20      |           |          |         |
| 158.0 | SPI0_D1      | J10-21 | 21      |           | spi_miso | spi_miso|
| 1A4.1 | MCASP0_FSR   | J11-34 | 22      | irc_b (n) |          | irc_b   |
| 150.0 | SPI0_SCLK    | J10-17 | 23      |           | spi_clk  | spi_clk |
|       |              |        | 24      | irc_b (o) |          |         |
|       |              |        | 25      |           |          |         |
| 15C.0 | SPI0_CS0     | J10-15 | 26      | irc_b (o) | spi_ce   | spi_ce  |
|       |              |        | 27      |           |          |         |
|       |              |        | 28      |           |          |         |
|       |              |        | 29      |           |          |         |
|       |              |        | 30      |           |          |         |
|       |              |        | 31      |           |          |         |
| 230.6 | UART3_CSTn   | J10-12 | 32      |           |          | pwm3    |
| 228.6 | UART3_RXD    | J10-8  | 33      |           |          | pwm2    |
|       |              |        | 34      |           |          |         |
| 04C.7 | MMC2_DAT2    | J11-35 | 35      |           |          | pwm2_en |
|       |              |        | 36      |           |          |         |
| 078.7 | MMC2_DAT3    | J11-37 | 37      |           |          | pwm3_en |
|       |              |        | 38      |           |          |         |
|       |              |        | 39      |           |          |         |
|       |              |        | 40      |           |          |         |

