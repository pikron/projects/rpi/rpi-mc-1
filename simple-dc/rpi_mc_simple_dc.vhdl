--
-- * LXPWR slave part *
--  common sioreg & common counter for several ADC&PWM blocks
--
-- part of LXPWR motion control board (c) PiKRON Ltd
-- idea by Pavel Pisa PiKRON Ltd <pisa@cmp.felk.cvut.cz>
-- code by Marek Peca <mp@duch.cz>
-- 01/2013
--
-- license: GNU GPLv3
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.util.all;

entity rpi_mc_simple_dc is
  port (
    gpio2: in std_logic; -- SDA
    gpio3: in std_logic; -- SCL
    gpio4: in std_logic; -- CLK
    gpio14: in std_logic; -- Tx
    gpio15: in std_logic; -- Rx
    gpio17: in std_logic; -- RTS
    gpio18: in std_logic; -- PWM0/PCMCLK
    gpio27: in std_logic; -- SD1DAT3
    gpio22: in std_logic; -- SD1CLK
    gpio23: out std_logic; -- SD1CMD
    gpio24: out std_logic; -- SD1DAT0
    gpio10: in std_logic; -- SPI0MOSI
    gpio9: in std_logic; -- SPI0MISO
    gpio25: in std_logic; -- SD1DAT1
    gpio11: in std_logic; -- SPI0SCLK
    gpio8: out std_logic; -- SPI0CE0
    gpio7: out std_logic; -- SPI0CE1
    gpio5: in std_logic; -- GPCLK1
    gpio6: in std_logic; -- GPCLK2
    gpio12: in std_logic; -- PWM0
    gpio13: in std_logic; -- PWM1
    gpio19: in std_logic; -- PWM1/SPI1MISO/PCMFS
    gpio16: in std_logic; -- SPI1CE2
    gpio26: in std_logic; -- SD1DAT2
    gpio20: in std_logic; -- SPI1MOSI/PCMDIN/GPCLK0
    gpio21: in std_logic; -- SPI1SCLK/PCMDOUT/GPCLK1
    --
    -- PWM
    -- Each PWM signal has cooresponding shutdown
    pwm: out std_logic_vector (1 to 3);
    shdn: out std_logic_vector (1 to 3);
    -- Fault/power stage status
    stat: in std_logic_vector (1 to 3);
    -- HAL inputs
    hal_in: in std_logic_vector (1 to 3);
    -- IRC inputs
    irc_a: in std_logic;
    irc_b: in std_logic;
    irc_i: in std_logic;
    -- Power status
    power_stat: in std_logic;
    -- ADC for current
    adc_miso: in std_logic;
    adc_mosi: in std_logic;
    adc_sclk: in std_logic;
    adc_scs: in std_logic;
    -- Extarnal SPI
    ext_miso: in std_logic;
    ext_mosi: in std_logic;
    ext_sclk: in std_logic;
    ext_scs0: in std_logic;
    ext_scs1: in std_logic;
    ext_scs2: in std_logic;
    -- RS-485 Transceiver
    rs485_rxd: in std_logic;
    rs485_txd: out std_logic;
    rs485_dir: out std_logic;
    -- CAN Transceiver
    can_rx: in std_logic;
    can_tx: in std_logic;
    -- DIP switch
    dip_sw: in std_logic_vector (1 to 3);
    -- Unused terminal to keep design tools silent
    dummy_unused : out std_logic
    );
end rpi_mc_simple_dc;

architecture behavioral of rpi_mc_simple_dc is
attribute syn_noprune :boolean;
attribute syn_preserve :boolean;
attribute syn_keep :boolean;
attribute syn_hier :boolean;
  -- Actel lib
  -- component pll50to200
  --   port (
  --     powerdown, clka: in std_logic;
  --     lock, gla: out std_logic
  --   );
  -- end component;
  -- component CLKINT
  --   port (A: in std_logic; Y: out std_logic);
  -- end component;
  --
  signal pwm_in, pwm_dir_in: std_logic;

--  attribute syn_noprune of gpio2 : signal is true;
--  attribute syn_preserve of gpio2 : signal is true;
--  attribute syn_keep of gpio2 : signal is true;
--  attribute syn_hier of gpio2 : signal is true;

begin
-- PLL as a reset generator
--   copyclk: CLKINT
--      port map (
--        a => clkm,
--        y => pll_clkin);
--   pll: pll50to200
--     port map (
--       powerdown => '1',
--       clka => pll_clkin,
--       gla => pll_clkout,
--       lock => pll_lock);
-- --  reset <= not pll_lock;
--   reset <= '0';                         -- TODO: apply reset for good failsafe
                                           -- upon power-on
--   clock <= clkm;

  dummy_unused <= gpio2 and gpio3 and gpio4 and
     gpio5 and gpio6 and gpio9 and
     gpio10 and gpio11 and gpio12 and gpio13 and gpio14 and
     gpio15 and gpio16 and gpio17 and gpio19 and
     gpio20 and gpio21 and
     gpio25 and gpio26 and gpio27 and
     stat(1) and stat(2) and stat(3) and
     hal_in(1) and hal_in(2) and hal_in(3) and
     irc_i and power_stat and adc_miso and adc_mosi and adc_sclk and adc_scs and
     ext_miso and ext_mosi and ext_sclk and ext_scs0 and ext_scs1 and ext_scs2 and
     rs485_rxd and
     can_rx and can_tx and
     dip_sw(1) and dip_sw(2) and dip_sw(3);

  rs485_txd <= '1';
  rs485_dir <= '0';

  gpio23 <= irc_a;
  gpio24 <= irc_a;

  gpio7 <= irc_b;
  gpio8 <= irc_b;

  pwm_in <= gpio18;
  pwm_dir_in <= gpio22;

  shdn(1) <= '0';
  shdn(2) <= '0';
  shdn(3) <= '1';

  pwm(1) <= pwm_in and not pwm_dir_in;
  pwm(2) <= pwm_in and pwm_dir_in;
  pwm(3) <= '0';

end behavioral;
