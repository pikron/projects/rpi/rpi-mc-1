# synplify_pro -licensetype synplifypro_actel -batch syn.tcl

project -new rpi_mc_simple_dc
impl -name syn0

#add_file pll50to200.vhd
add_file util.vhdl

# top-level
add_file rpi_mc_simple_dc.vhdl
#add_file rpi_mc_simple_dc.sdc

set_option -technology IGLOO
set_option -part AGL250V5
set_option -package VQFP100
set_option -speed_grade std

set_option -frequency 40.0

project -run
project -save
